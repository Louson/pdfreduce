#! /bin/sh

OPTS=$(getopt -o "m:o:fh" --long "mode:,output:,force,help" -- "$@")
eval set -- "$OPTS"
unset OPTS

TMPOUT="/tmp/pdfreduced.pdf"
OUTPUT="./out.pdf"

OPT_MODE="NORMAL"
FORCE=

function usage() {
    echo "Usage: $0 [-mofh] <file>"
    echo "  -f, --force force questions to yes"
    echo "  -h, --help print this help"
    echo "  -m, --mode [EASY|NORMAL|STRONG] set the optimization mode"
    echo "  -o, --output <output> set the output file"
}

function main() {
    while true; do
	case "$1" in
	    '-m'|'--mode')
		OPT_MODE=$2
		shift 2
		;;
	    '-o'|'--output')
		OUTPUT=$2
		shift 2
		;;
	    '-f'|'--force')
		FORCE="1"
		shift
		;;
	    '-h'|'--help')
		usage
		shift
		exit 0
		;;
	    '--')
		shift
		break
		;;
	    *)
		echo 'Internal error!' >&2
		exit 22
		;;
	esac
    done

    if [ $# -ne 1 ]; then
	echo 'Incorrect input file' >&2
	usage
	exit 22
    fi

    INPUT=$1

    case $OPT_MODE in
	"EASY") pdfreduce_easy $INPUT $TMPOUT ;;
	"NORMAL") pdfreduce_normal $INPUT $TMPOUT ;;
	"STRONG") pdfreduce_strong $INPUT $TMPOUT ;;
	*) echo "Incorrect mode $OPT_MODE" && exit 22;;
    esac
    echo "Writing to $OUTPUT"

    if [ $? -eq 0 ]; then
	if [ -f "$OUTPUT" ] && [ -z "$FORCE" ]; then
	    yes_or_No "$OUTPUT already exist. Erase ?" || exit 5
	fi

	IN_SIZE=$(stat --printf="%s" $INPUT)
	OUT_SIZE=$(stat --printf="%s" $TMPOUT)
	echo "Input size: $IN_SIZE"
	echo "Ouput size: $OUT_SIZE"
	if [ $OUT_SIZE -ge $IN_SIZE ]; then
	    echo "Output isn't smaller than input. Abort"
	    rm -f $TMPOUT
	    exit 0
	fi

	mv $TMPOUT $OUTPUT
    fi
}

function pdfreduce_easy {
    gs -q -dNOPAUSE -dBATCH -sSAFER \
       -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/printer \
       -dEmbedAllFonts=true \
       -dSubsetFonts=true \
       -dColorImageDownsampleType=/Bicubic \
       -dColorImageResolution=72 \
       -dGrayImageDownsampleType=/Bicubic \
       -dGrayImageResolution=72 \
       -dMonoImageDownsampleType=/Bicubic \
       -dMonoImageResolution=72 \
       -sOutputFile=$2 \
       $1
}

function pdfreduce_normal {
    gs -q -dNOPAUSE -dBATCH -sSAFER \
       -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/ebook \
       -dEmbedAllFonts=true \
       -dSubsetFonts=true \
       -dColorImageDownsampleType=/Bicubic \
       -dColorImageResolution=72 \
       -dGrayImageDownsampleType=/Bicubic \
       -dGrayImageResolution=72 \
       -dMonoImageDownsampleType=/Bicubic \
       -dMonoImageResolution=72 \
       -sOutputFile=$2 \
       $1
}

function pdfreduce_strong {
    gs -q -dNOPAUSE -dBATCH -dSAFER \
       -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/screen \
       -dEmbedAllFonts=true \
       -dSubsetFonts=true \
       -dColorImageDownsampleType=/Bicubic \
       -dColorImageResolution=72 \
       -dGrayImageDownsampleType=/Bicubic \
       -dGrayImageResolution=72 \
       -dMonoImageDownsampleType=/Bicubic \
       -dMonoImageResolution=72 \
       -sOutputFile=$2 \
       $1
}

function yes_or_No {
    while true; do
        read -p "$* [y/N]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            *) return  1 ;;
        esac
    done
}

main $*
